package com.andru.libs.tracksystem;

public class TrackObject {
	public int id = -1;
	public float x, y;
	public float px, py;
	public float velx, vely;
	public int w;
	public int h;
	public int c;
	public int mass;
	float time;

	public TrackObject() {
	}

	public int compareById(TrackObject o) {
		Integer id1 = new Integer(id);
		Integer id2 = new Integer(o.id);
		int comp = id1.compareTo(id2);
		return comp;
	}
}
